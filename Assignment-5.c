#include<stdio.h>

#define PerCost 500
#define PerAtt 3

//Funtions
int NOA(int price);
int In(int price);
int cost(int price);
int profit(int price);


int	NOA(int price){
	return 120-(price-15)/5*20;
}

int In(int price){
	return NOA(price)*price;
}

int cost(int price){
	return NOA(price)*PerAtt+PerCost;
}

int profit(int price){
	return In(price)-cost(price);
}

int main(){
	int price;
	printf("\nExpected Profit for Ticket Prices: \n\n");
	for(price=5;price<50;price+=5)
	{
		printf("Ticket Price = Rs.%d \t\t Profit = Rs.%d\n",price,profit(price));
		printf("\n");
    }
		return 0;
	}




